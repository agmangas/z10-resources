# coding=utf-8
import json
import logging
import logging.handlers
import copy
import random
import math
import csv


def get_logger():
    return logging.getLogger()


def configure_logging():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)


class Rarity(object):
    BASIC = "BASIC"
    MEDIUM = "MEDIUM"
    ADVANCED = "ADVANCED"
    EPIC = "EPIC"

    @classmethod
    def list(cls):
        return [
            cls.BASIC,
            cls.MEDIUM,
            cls.ADVANCED,
            cls.EPIC
        ]


class ResourceMaterial(object):
    WOOD = "WOOD"
    MINERAL = "MINERAL"
    VEGETAL = "VEGETAL"
    ETHER = "ETHER"

    @classmethod
    def totals(cls):
        return {
            cls.WOOD: 90,
            cls.MINERAL: 90,
            cls.VEGETAL: 90,
            cls.ETHER: 90
        }

    @classmethod
    def totals_resource_set(cls):
        totals = cls.totals()
        ret = ResourcesSet()
        for key, val in totals.iteritems():
            ret.update(key, val)

        return ret

    @classmethod
    def list(cls):
        return [
            cls.WOOD,
            cls.MINERAL,
            cls.VEGETAL,
            cls.ETHER
        ]

    @classmethod
    def count(cls):
        totals = cls.totals()
        totals.pop(cls.ETHER)

        return sum([item[1] for item in totals.iteritems()])

    @classmethod
    def name(cls, val):
        names_map = {
            cls.WOOD: "Madera",
            cls.MINERAL: "Mineral",
            cls.VEGETAL: "Vegetal",
            cls.ETHER: "Eter"
        }

        return names_map.get(val)


class ResourceSpecial(object):
    RED = "RED"
    WHITE = "WHITE"
    GREEN = "GREEN"
    BLUE = "BLUE"
    BLACK = "BLACK"

    @classmethod
    def totals(cls):
        return {
            cls.RED: 150,
            cls.WHITE: 150,
            cls.GREEN: 100,
            cls.BLUE: 50,
            cls.BLACK: 50
        }

    @classmethod
    def totals_resource_set(cls):
        totals = cls.totals()
        ret = ResourcesSet()
        for key, val in totals.iteritems():
            ret.update(key, val)

        return ret

    @classmethod
    def list(cls):
        return [
            cls.RED,
            cls.WHITE,
            cls.GREEN,
            cls.BLUE,
            cls.BLACK
        ]

    @classmethod
    def count(cls):
        totals = cls.totals()
        totals.pop(cls.BLACK)

        return sum([item[1] for item in totals.iteritems()])

    @classmethod
    def name(cls, val):
        names_map = {
            cls.RED: "Mercurio",
            cls.WHITE: "Plata electrica",
            cls.GREEN: "Madera ferrea",
            cls.BLUE: "Carbon liquido",
            cls.BLACK: "Cristal conductor"
        }

        return names_map.get(val)


class ResourceSetBuilder(object):
    DEFAULT_MAX_RAND_DEVIATION = 0.25

    BASIC_PERCENT_MATERIAL = 0.02
    BASIC_PERCENT_SPECIAL = 0.01

    MEDIUM_PERCENT_MATERIAL = 0.03
    MEDIUM_PERCENT_SPECIAL = 0.03

    ADVANCED_PERCENT_MATERIAL = 0.07
    ADVANCED_PERCENT_SPECIAL = 0.07

    EPIC_PERCENT_MATERIAL = 0.15
    EPIC_PERCENT_SPECIAL = 0.15

    @classmethod
    def add_deviation(cls, val, max_deviation=DEFAULT_MAX_RAND_DEVIATION):
        deviation = random.uniform(-max_deviation, max_deviation)

        return val + val * deviation

    @classmethod
    def gen_distribution_percent(cls, num_shares, suggested_shares=None,
                                 max_deviation=DEFAULT_MAX_RAND_DEVIATION):
        assert suggested_shares is None or sum(suggested_shares) <= 1.0

        ret = list()

        for idx in range(num_shares):
            if isinstance(suggested_shares, list) and len(suggested_shares) > idx:
                share = cls.add_deviation(suggested_shares[idx], max_deviation=max_deviation)
            elif not len(ret):
                share = random.uniform(0.0, 1.0)
            else:
                percent_left = 1.0 - sum(ret)
                share = percent_left * random.uniform(0.0, 1.0)

            if idx + 1 == num_shares:
                share = 1.0 - sum(ret) if len(ret) else 1.0

            assert sum(ret) + share <= 1.0

            ret.append(share)

        return ret

    @classmethod
    def update_resource_set(cls, resource_set, types, total_count, distribution_shares):
        assert len(distribution_shares) == len(types)
        assert total_count > 0

        for idx, the_type in enumerate(types):
            distrib_percent = distribution_shares[idx]
            item_count = math.floor(total_count * distrib_percent)
            resource_set.update(the_type, item_count)

    @classmethod
    def build_resource_set(cls, formula_set, num_types_mat, num_types_spe, percent_mat, percent_spe,
                           suggested_shares_mat=None, suggested_shares_spe=None, formula=None):
        ret = ResourcesSet()

        pref_mat = formula_set.get_preference_order_material()
        pref_spe = formula_set.get_preference_order_special()

        if formula and formula.suggested_resources:
            sugg_mat = filter(lambda val: val in ResourceMaterial.list(), formula.suggested_resources)
            sugg_spe = filter(lambda val: val in ResourceSpecial.list(), formula.suggested_resources)
            pref_mat = (sugg_mat + pref_mat)[:len(pref_mat)]
            pref_spe = (sugg_spe + pref_spe)[:len(pref_spe)]
            print(sugg_mat, sugg_spe)
            print(pref_mat, pref_spe)

        materials = pref_mat[:num_types_mat]
        specials = pref_spe[:num_types_spe]

        total_mat = int(ResourceMaterial.count() * cls.add_deviation(percent_mat))
        total_spe = int(ResourceSpecial.count() * cls.add_deviation(percent_spe))

        distrib_mat = cls.gen_distribution_percent(
            len(materials), suggested_shares=suggested_shares_mat)

        distrib_spe = cls.gen_distribution_percent(
            len(specials), suggested_shares=suggested_shares_spe)

        cls.update_resource_set(ret, materials, total_mat, distrib_mat)
        cls.update_resource_set(ret, specials, total_spe, distrib_spe)

        return ret

    @classmethod
    def build_basic(cls, formula_set, formula=None):
        num_types_mat = 3
        num_types_spe = 3

        suggested_shares_mat = [0.4]
        suggested_shares_spe = [0.4]

        min_black = 0.0
        min_ether = 0.0
        max_black = 0.0
        max_ether = 0.0

        resource_set = cls.build_resource_set(
            formula_set, num_types_mat, num_types_spe,
            cls.BASIC_PERCENT_MATERIAL, cls.BASIC_PERCENT_SPECIAL,
            suggested_shares_mat=suggested_shares_mat,
            suggested_shares_spe=suggested_shares_spe,
            formula=formula)

        count_black = int(random.uniform(min_black, max_black))
        count_ether = int(random.uniform(min_ether, max_ether))

        resource_set.update(ResourceMaterial.ETHER, count_ether)
        resource_set.update(ResourceSpecial.BLACK, count_black)

        return resource_set

    @classmethod
    def build_medium(cls, formula_set, formula=None):
        num_types_mat = 3
        num_types_spe = 3

        suggested_shares_mat = [0.6]
        suggested_shares_spe = [0.6]

        min_black = 0.0
        min_ether = 0.0
        max_black = 3.0
        max_ether = 3.0

        resource_set = cls.build_resource_set(
            formula_set, num_types_mat, num_types_spe,
            cls.MEDIUM_PERCENT_MATERIAL, cls.MEDIUM_PERCENT_SPECIAL,
            suggested_shares_mat=suggested_shares_mat,
            suggested_shares_spe=suggested_shares_spe,
            formula=formula)

        count_black = int(random.uniform(min_black, max_black))
        count_ether = int(random.uniform(min_ether, max_ether))

        resource_set.update(ResourceMaterial.ETHER, count_ether)
        resource_set.update(ResourceSpecial.BLACK, count_black)

        return resource_set

    @classmethod
    def build_advanced(cls, formula_set, formula=None):
        num_types_mat = 3
        num_types_spe = 4

        suggested_shares_mat = [0.5, 0.3]
        suggested_shares_spe = [0.5, 0.3]

        min_black = 2.0
        min_ether = 2.0
        max_black = 6.0
        max_ether = 6.0

        resource_set = cls.build_resource_set(
            formula_set, num_types_mat, num_types_spe,
            cls.ADVANCED_PERCENT_MATERIAL, cls.ADVANCED_PERCENT_SPECIAL,
            suggested_shares_mat=suggested_shares_mat,
            suggested_shares_spe=suggested_shares_spe,
            formula=formula)

        count_black = int(random.uniform(min_black, max_black))
        count_ether = int(random.uniform(min_ether, max_ether))

        resource_set.update(ResourceMaterial.ETHER, count_ether)
        resource_set.update(ResourceSpecial.BLACK, count_black)

        return resource_set

    @classmethod
    def build_epic(cls, formula_set, formula=None):
        num_types_mat = 3
        num_types_spe = 4

        suggested_shares_mat = [0.4, 0.3]
        suggested_shares_spe = [0.3, 0.3]

        min_black = 4.0
        min_ether = 4.0
        max_black = 8.0
        max_ether = 8.0

        resource_set = cls.build_resource_set(
            formula_set, num_types_mat, num_types_spe,
            cls.EPIC_PERCENT_MATERIAL, cls.EPIC_PERCENT_SPECIAL,
            suggested_shares_mat=suggested_shares_mat,
            suggested_shares_spe=suggested_shares_spe,
            formula=formula)

        count_black = int(random.uniform(min_black, max_black))
        count_ether = int(random.uniform(min_ether, max_ether))

        resource_set.update(ResourceMaterial.ETHER, count_ether)
        resource_set.update(ResourceSpecial.BLACK, count_black)

        return resource_set

    @classmethod
    def build(cls, rarity, formula_set, formula=None):
        rarity_method_map = {
            Rarity.BASIC: cls.build_basic,
            Rarity.MEDIUM: cls.build_medium,
            Rarity.ADVANCED: cls.build_advanced,
            Rarity.EPIC: cls.build_epic
        }

        return rarity_method_map[rarity](formula_set, formula=formula)


class ResourcesSet(object):
    @classmethod
    def get_total_set(cls):
        total = ResourcesSet()

        for key, val in ResourceSpecial.totals().iteritems():
            total.update(key, val)

        for key, val in ResourceMaterial.totals().iteritems():
            total.update(key, val)

        return total

    def __init__(self):
        self.materials = {}
        self.specials = {}

    def __str__(self):
        material_strs = [
            "{}: {}".format(ResourceMaterial.name(key), val)
            for key, val in self.materials.iteritems()]

        special_strs = [
            "{}: {}".format(ResourceSpecial.name(key), val)
            for key, val in self.specials.iteritems()]

        all_strs = material_strs + special_strs

        return "\n".join(all_strs)

    def get_sorted_tuple_materials(self):
        tuple_lst = [(key, val) for key, val in self.materials.iteritems()]

        return sorted(tuple_lst, key=lambda item: item[1])

    def get_sorted_tuple_specials(self):
        tuple_lst = [(key, val) for key, val in self.specials.iteritems()]

        return sorted(tuple_lst, key=lambda item: item[1])

    def update(self, resource_type, value):
        if int(value) == 0:
            return

        if resource_type in ResourceMaterial.list():
            self.materials[resource_type] = int(value)
        elif resource_type in ResourceSpecial.list():
            self.specials[resource_type] = int(value)
        else:
            raise ValueError("Unknown resource type")

    def is_empty(self):
        return not len(self.materials) and not len(self.specials)

    def __add__(self, other):
        assert isinstance(other, ResourcesSet)

        self_copy = copy.copy(self)

        for key, val in other.materials.iteritems():
            self_copy.materials[key] = self_copy.materials.get(key, 0) + val

        for key, val in other.specials.iteritems():
            self_copy.specials[key] = self_copy.specials.get(key, 0) + val

        return self_copy

    def __sub__(self, other):
        assert isinstance(other, ResourcesSet)

        self_copy = copy.copy(self)

        def _subtract_from_dict(the_dict, the_key, the_val):
            the_dict[the_key] = the_dict.get(the_key, 0) - the_val

        for key, val in other.materials.iteritems():
            _subtract_from_dict(self_copy.materials, key, val)

        for key, val in other.specials.iteritems():
            _subtract_from_dict(self_copy.specials, key, val)

        return self_copy


class Formula(object):
    def __init__(self, name, rarity, character=None,
                 description=None, formula_format=None,
                 cost=None, is_potion=False, suggested_resources=None):
        assert rarity in Rarity.list()

        self._rarity = None
        self._cost = None

        self.rarity = rarity
        self.cost = cost

        self.name = name
        self.character = character
        self.description = description
        self.formula_format = formula_format
        self.is_potion = is_potion
        self.suggested_resources = suggested_resources

    def __unicode__(self):
        return u"{} :: {}\n==========\n{}".format(self.rarity, self.name, self.cost)

    def __str__(self):
        return unicode(self).encode("utf-8")

    def has_cost(self):
        return self.cost is not None

    @property
    def rarity(self):
        return self._rarity

    @rarity.setter
    def rarity(self, value):
        assert value in Rarity.list()
        self._rarity = value

    @property
    def cost(self):
        return self._cost

    @cost.setter
    def cost(self, value):
        if value is None:
            return

        assert isinstance(value, ResourcesSet)
        self._cost = value

    def add_cost(self, new_cost):
        if self.cost is not None:
            self.cost += new_cost
        else:
            self.cost = new_cost


class FormulaSet(object):
    DEFAULT_FILE_PATH = './formulas.json'

    @classmethod
    def from_json(cls, file_path=DEFAULT_FILE_PATH):
        get_logger().info("Reading file: {}".format(file_path))

        with open(file_path, 'r') as fhandler:
            parsed_formulas = json.loads(fhandler.read())

        formula_set = FormulaSet()

        for item in parsed_formulas:
            formula = Formula(
                item["name"], item["rarity"],
                character=item.get("character"),
                description=item.get("description"),
                formula_format=item.get("format"),
                is_potion=item.get("isPotion", False),
                suggested_resources=item.get('suggestedResources', []))

            formula_set.append(formula)

        return formula_set

    def __init__(self):
        self.formulas = []

    def __str__(self):
        formulas_str = "\n\n".join([str(formula) for formula in self.formulas])

        return "### Formula Set\n##########\n\n{}".format(formulas_str)

    def get_preference_order_material(self):
        resource_set_current = self.get_current_total_resource_set()
        resource_set_totals = ResourceMaterial.totals_resource_set()
        resource_set_left = resource_set_totals - resource_set_current

        sorted_tuples = resource_set_left.get_sorted_tuple_materials()
        sorted_tuples.reverse()
        sorted_tuples = filter(lambda tup: tup[0] != ResourceMaterial.ETHER, sorted_tuples)

        return [item[0] for item in sorted_tuples]

    def get_preference_order_special(self):
        resource_set_current = self.get_current_total_resource_set()
        resource_set_totals = ResourceSpecial.totals_resource_set()
        resource_set_left = resource_set_totals - resource_set_current

        sorted_tuples = resource_set_left.get_sorted_tuple_specials()
        sorted_tuples.reverse()
        sorted_tuples = filter(lambda tup: tup[0] != ResourceSpecial.BLACK, sorted_tuples)

        return [item[0] for item in sorted_tuples]

    def append(self, formula):
        assert isinstance(formula, Formula)
        self.formulas.append(formula)

    def get_current_total_resource_set(self):
        total = ResourcesSet()

        for formula in self.formulas:
            if formula.has_cost():
                total += formula.cost

        return total

    def build_resource_sets(self):
        logger = get_logger()

        for formula in self.formulas:
            resource_set = ResourceSetBuilder.build(formula.rarity, self, formula=formula)
            formula.add_cost(resource_set)
            logger.debug("Updating formula:\n{}".format(formula))

    def export_to_csv(self, file_path):
        fieldnames = [
            "nombre",
            "nivel",
            "personaje",
            "descripcion",
            "formato",
            "pocion",
            "coste"
        ]

        with open(file_path, 'w') as csvfile:
            csv_writer = csv.DictWriter(csvfile, fieldnames, delimiter=";")
            csv_writer.writeheader()

            for formula in self.formulas:
                row_name = formula.name.encode("utf-8")
                row_char = formula.character.encode("utf-8") if formula.character else ""
                row_descr = formula.description.encode("utf-8") if formula.description else ""
                row_format = formula.formula_format.encode("utf-8") if formula.formula_format else ""
                row_is_potion = "SI" if formula.is_potion else "NO"

                csv_writer.writerow({
                    "nombre": row_name,
                    "nivel": formula.rarity,
                    "personaje": row_char,
                    "descripcion": row_descr,
                    "formato": row_format,
                    "pocion": row_is_potion,
                    "coste": formula.cost
                })


def main():
    configure_logging()
    logger = get_logger()

    logger.info('Loading FormulaSet from JSON')

    formula_set = FormulaSet.from_json()

    logger.info("Building FormulaSet")

    formula_set.build_resource_sets()
    total_set = formula_set.get_current_total_resource_set()

    logger.info("Total ResourceSet:\n{}".format(total_set))

    formula_set.export_to_csv("output.csv")


if __name__ == "__main__":
    main()
